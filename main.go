package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/spf13/viper"
)

func init() {
	viper.SetConfigName("rrnotify")                    // name of config file (without extension)
	viper.AddConfigPath("$HOME/.config/reprap_status") // call multiple times to add many search paths
	viper.AddConfigPath(".")                           // call multiple times to add many search paths
	err := viper.ReadInConfig()                        // Find and read the config file
	if err != nil {                                    // Handle errors reading the config file
		panic(err)
	}

	viper.WatchConfig()
	viper.SetConfigType("yaml")
}

func main() {
	var status rrStatusType
	var fileinfo rrFileinfoType
	var notify = make(map[string]string)
	oldErr := errors.New(``)

	go func() {
		httpServer()
	}()

	for {
		rrStatus, err := downloadData(fmt.Sprintf("http://%v/rr_status?type=1", viper.Get(`connection.host`)))
		if err != nil {
			if oldErr.Error() != err.Error() {
				log.Printf("Error while downloading rr_status: %v. Will hide consequent errors...", err)
			}
			oldErr = err
			notify = make(map[string]string)
			continue
		}
		err = json.Unmarshal(rrStatus, &status)
		if err != nil {
			log.Printf("Error parsing JSON: %v", err)
			continue
		}
		rrFileInfo, err := downloadData(fmt.Sprintf("http://%v/rr_fileinfo", viper.Get(`connection.host`)))
		if err != nil {
			if oldErr.Error() != err.Error() {
				log.Printf("Error while downloading rr_fileinfo: %v. Will hide consequent errors...", err)
			}
			oldErr = err
			notify = make(map[string]string)
			continue
		}
		err = json.Unmarshal(rrFileInfo, &fileinfo)
		if err != nil {
			log.Printf("Error parsing JSON: %v", err)
			continue
		}
		notify[`old_file`] = notify[`current_file`]
		notify[`current_file`] = fileinfo.FileName

		if status.Status == `P` && notify[`status`] != status.Status {
			log.Printf("Print status: printing\n")
			if notify[`notified_for_printing`] != `yes` {
				log.Printf("Will send notification if any configured: %v, %v, %v\n", notify[`old_file`], notify[`current_file`], notify[`notified_for_printing`])
				sendNotification(status, fileinfo)
				notify[`notified_for_printing`] = `yes`
			}
			if notify[`action_for_printing`] != `yes` {
				log.Printf("Will run any commands if defined: %v, %v, %v\n", notify[`old_file`], notify[`current_file`], notify[`action_for_printing`])
				runCommand(status)
				notify[`action_for_printing`] = `yes`
			}
			notify[`status`] = status.Status

		} else if status.Status == `S` && notify[`status`] != status.Status {
			log.Printf("Print status: paused\n")
			if notify[`notified_for_paused`] != `yes` {
				log.Printf("Will send notification if any configured: %v, %v, %v\n", notify[`old_file`], notify[`current_file`], notify[`notified_for_paused`])
				sendNotification(status, fileinfo)
				notify[`notified_for_paused`] = `yes`
			}
			if notify[`action_for_paused`] != `yes` {
				log.Printf("Will run any commands if defined: %v, %v, %v\n", notify[`old_file`], notify[`current_file`], notify[`action_for_paused`])
				runCommand(status)
				notify[`action_for_paused`] = `yes`
			}
			notify[`status`] = status.Status

		} else if status.Status == `I` && (notify[`status`] == `S` || notify[`status`] == `P`) {
			log.Printf("Print status: finished\n")
			log.Printf("Will send notification if any configured: %v, %v\n", notify[`old_file`], notify[`current_file`])
			sendNotification(status, fileinfo)
			log.Printf("Will run any commands if defined: %v, %v\n", notify[`old_file`], notify[`current_file`])
			runCommand(status)
			notify = make(map[string]string)
			notify[`status`] = status.Status

		}
		time.Sleep(viper.GetDuration(`connection.polling_time`) * time.Second)
	}
}

func downloadData(url string) ([]byte, error) {
	var body []byte
	var err error
	var errCount int64

	for {
		client := http.Client{
			Timeout: time.Duration(5 * time.Second),
		}

		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Printf("Unable to create a new http request: %v", err)
		}

		res, err := client.Do(req)
		if err != nil {
			errCount = errCount + 1
		} else {
			defer res.Body.Close()

			body, err = ioutil.ReadAll(res.Body)
			if err != nil {
				errCount = errCount + 1
				log.Printf("Error while reading JSON data: %v", err)
			} else {
				errCount = 0
				break
			}
		}
		if errCount > viper.GetInt64(`connection.error_count`) {
			errCount = 0
			return nil, errors.New(`Too many errors, resetting counters and stoping track operation`)
		}
		time.Sleep(viper.GetDuration(`connection.polling_time`) * time.Second)
	}
	return body, err
}

func returnStatus(shortStatus string) string {
	switch shortStatus {
	case `P`:
		return `printing`
	case `S`:
		return `paused`
	case `I`:
		return `finished`
	}
	return ``
}
