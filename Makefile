all: linux darwin freebsd windows linux-arm

linux:
	CGO_ENABLED=0 GOARCH=amd64 GOOS=linux go build -a -tags netgo -ldflags '-extldflags "-static"' -o ./artifacts/reprap_notify_linux_amd64

darwin:
	CGO_ENABLED=0 GOARCH=amd64 GOOS=darwin go build -a -ldflags '-s -extldflags "-sectcreate __TEXT __info_plist Info.plist"' -o ./artifacts/reprap_notify_darwin_amd64

freebsd:
	CGO_ENABLED=0 GOARCH=amd64 GOOS=freebsd go build -a -tags netgo -ldflags '-extldflags "-static"' -o ./artifacts/reprap_notify_freebsd_amd64

windows:
	CGO_ENABLED=0 GOARCH=amd64 GOOS=windows go build -a -tags netgo -ldflags '-extldflags "-static"' -o ./artifacts/reprap_notify_windows_amd64.exe

linux-arm:
	CGO_ENABLED=0 GOARCH=arm GOOS=linux go build -a -o ./artifacts/reprap_notify_linux_arm
