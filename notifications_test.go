package main

import (
	"testing"
)

func Test_niceTime(t *testing.T) {
	type args struct {
		sec int
	}

	tests := []struct {
		name    string
		args    args
		wantRes string
	}{
		{name: `Weeks`, args: args{sec: 34149910}, wantRes: `56w 3d 6h 5m 10s`},
		{name: `Hours`, args: args{sec: 49910}, wantRes: `13h 51m 50s`},
		{name: `Minutes`, args: args{sec: 65}, wantRes: `1m 5s`},
		{name: `Seconds`, args: args{sec: 55}, wantRes: `55s`},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotRes := niceTime(tt.args.sec); gotRes != tt.wantRes {
				t.Errorf("niceTime() = %v, want %v", gotRes, tt.wantRes)
			}
		})
	}
}

func Test_contains(t *testing.T) {
	type args struct {
		search string
		list   []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: `TrueTest`, args: args{search: `semetary`, list: []string{`semetary`}}, want: true},
		{name: `FalseTest`, args: args{search: `cemetary`, list: []string{`semetary`}}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := contains(tt.args.search, tt.args.list); got != tt.want {
				t.Errorf("contains() = %v, want %v", got, tt.want)
			}
		})
	}
}
