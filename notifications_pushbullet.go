package main

import (
	"log"

	"github.com/spf13/viper"
	"github.com/xconstruct/go-pushbullet"
)

func sendPushbullet(messageTitle string, messageText string) {
	pb := pushbullet.New(viper.GetString(`notifications.pushbullet.apikey`))

	if viper.IsSet(`notifications.pushbullet.device`) && viper.GetString(`notifications.pushbullet.device`) != `` {
		dev, err := pb.Device(viper.GetString(`notifications.pushbullet.device`))
		if err != nil {
			log.Printf(`Unknown error occured while trying to setup pushbullet. Check if your API key or device name are ok. Error: %v`, err)
		}

		err = dev.PushNote(messageTitle, messageText)
		if err != nil {
			log.Printf(`Unknown error occured while trying to send pushbullet notification. Error: %v`, err)
		}
	} else {
		devs, err := pb.Devices()
		if err != nil {
			log.Printf(`Unknown error occured while trying to setup pushbullet. Check if your API key is ok. Error: %v`, err)
		}

		err = pb.PushNote(devs[0].Iden, messageTitle, messageText)
		if err != nil {
			log.Printf(`Unknown error occured while trying to send pushbullet notification. Error: %v`, err)
		}
	}
}
