package main

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
)

func sendNotification(status rrStatusType, fileinfo rrFileinfoType) {
	messageTitle := `Your friendly 3d printer status`
	messageText := fmt.Sprintf(`Status: %v, print file name: %v, print time: %v`, returnStatus(status.Status), fileinfo.FileName, niceTime(fileinfo.PrintDuration))

	if viper.GetBool(`notifications.pushbullet.enabled`) && viper.Get(`notifications.pushbullet.apikey`) != `` && contains(returnStatus(status.Status), viper.GetStringSlice(`notifications.pushbullet.when`)) {
		go func(messageText string, messageTitle string) {
			log.Printf("Sending pushbullet message\n")
			sendPushbullet(messageText, messageTitle)
		}(messageTitle, messageText)
	}

	if viper.GetBool(`notifications.pushover.enabled`) && viper.Get(`notifications.pushover.apikey`) != `` && contains(returnStatus(status.Status), viper.GetStringSlice(`notifications.pushover.when`)) {
		go func(messageText string, messageTitle string) {
			log.Printf("Sending pushover message\n")
			sendPushover(messageText, messageTitle)
		}(messageTitle, messageText)
	}

	if viper.GetBool(`notifications.email.enabled`) && viper.Get(`notifications.email.server`) != `` && contains(returnStatus(status.Status), viper.GetStringSlice(`notifications.pushover.when`)) {
		go func(messageText string, messageTitle string) {
			log.Printf("Sending email message\n")
			sendEmail(messageText, messageTitle)
		}(messageTitle, messageText)
	}

	if viper.GetBool(`notifications.telegram.enabled`) && viper.Get(`notifications.telegram.token`) != `` && viper.Get(`notifications.telegram.chatid`) != `` && contains(returnStatus(status.Status), viper.GetStringSlice(`notifications.pushover.when`)) {
		go func(messageText string, messageTitle string) {
			log.Printf("Sending telegram message\n")
			sendTelegram(messageText, messageTitle)
		}(messageTitle, messageText)
	}

	if viper.GetBool(`notifications.slack.enabled`) && viper.Get(`notifications.slack.webhookurl`) != `` && contains(returnStatus(status.Status), viper.GetStringSlice(`notifications.pushover.when`)) {
		go func(messageText string, messageTitle string) {
			log.Printf("Sending slack message\n")
			message := slackRequestBody{
				Attachments: []attachment{{
					Title: messageTitle,
					Text:  messageText,
					Color: "good",
				},
				},
			}
			sendSlackNotification(message)
		}(messageTitle, messageText)
	}
}

func contains(search string, list []string) bool {
	for _, v := range list {
		if search == v {
			return true
		}
	}
	return false
}

func niceTime(sec int) (res string) {
	wks, sec := sec/604800, sec%604800
	ds, sec := sec/86400, sec%86400
	hrs, sec := sec/3600, sec%3600
	mins, sec := sec/60, sec%60
	if wks != 0 {
		res += fmt.Sprintf("%dw ", wks)
	}
	if ds != 0 {
		res += fmt.Sprintf("%dd ", ds)
	}
	if hrs != 0 {
		res += fmt.Sprintf("%dh ", hrs)
	}
	if mins != 0 {
		res += fmt.Sprintf("%dm ", mins)
	}
	if sec != 0 {
		res += fmt.Sprintf("%ds", sec)
	}
	return
}
